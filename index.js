'use strict'

const express = require('express');
const app = express();
const axios = require('axios');

const debug = require('debug');
const error = debug('ddns:error');
const log = debug('ddns:log');
log.log = console.log.bind(console);

app.get('/', (req, res, next) => {
  res.sendStatus(200);
});

app.get('/update', async (req, res, next) => {
  try {
    const auth = await basicAuth(req.headers.authorization).catch((err) => {
      res.statusCode = 400;
      res.json({success: false, message: err});
    });
    if (!auth) return;

    const [zoneid, token] = auth;
    const hostname = req.query['hostname'];
    const ipaddr = req.query['ipaddr'];

    if (!hostname) {
      res.statusCode = 400;
      res.json({success: false, message: 'Missing hostname parameter'});
      return;
    } else if (!ipaddr) {
      res.statusCode = 400;
      res.json({success: false, message: 'Missing ipaddr parameter'});
      return;
    } else if (!/^(?=.*[^\.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/.test(ipaddr)) {
      res.statusCode = 400;
      res.json({success: false, message: 'Malformed ip address'});
      return;
    }

    const dnsResponse = await axios.get(`https://api.cloudflare.com/client/v4/zones/${zoneid}/dns_records`, {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }).catch(err => {
      if (err.response.status == 400) {
        res.statusCode = 400;
        res.json({success: false, message: 'Invalid zone id'});
      } else if (err.response.status = 403) {
        res.statusCode = 403;
        res.json({success: false, message: 'Invalid cloudflare token'});
      } else {
        error(err.response.data);
        res.statusCode = 500;
        res.json({success: false, message: 'Failed to get zone dns data from cloudflare'});
      }
    });
    if(!dnsResponse) return;
    const dnsRecords = dnsResponse.data.result;

    const existingRecord = dnsRecords.find(r => r.name == hostname);

    let dnsUpdateResponse;
    let oldip;
    if (existingRecord) {
      oldip = existingRecord.content;

      dnsUpdateResponse = await axios.patch(`https://api.cloudflare.com/client/v4/zones/${zoneid}/dns_records/${existingRecord.id}`, {
        'type': 'A',
        'name': hostname,
        'content': ipaddr,
        'ttl': 1
      }, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      }).catch(err => {
        // error(err.response.data);
        res.statusCode = 500;
        res.json({success: false, message: `Failed to update cloudflare dns record for ${hostname}`});
      });
    } else {
      dnsUpdateResponse = await axios.post(`https://api.cloudflare.com/client/v4/zones/${zoneid}/dns_records`, {
        'type': 'A',
        'name': hostname,
        'content': ipaddr,
        'ttl': 1,
        'proxied': false
      }, {
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json'
        }
      }).catch(err => {
        // error(err.response.data);
        res.statusCode = 500;
        res.json({success: false, message: `Failed to create cloudflare dns record for ${hostname}`});
      });
    }
    if (!dnsUpdateResponse) return;
    // log(`Successfully updated ${hostname} to ${ipaddr}`);

    if (oldip) {
      res.json({success: true, hostname: hostname, ip: ipaddr, before: oldip });
    } else {
      res.json({success: true, hostname: hostname, ip: ipaddr });
    }
  } catch (err) {
    error(err);
    res.statusCode = 500;
    res.json({success: false, message: 'Internal server error'});
  }
});

const port = process.env.PORT || 51264;
app.listen(port, log(`ddns listening on port ${port}`));

const basicAuth = auth => {
  return new Promise((resolve, reject) => {
    if (auth) {
      const [type, b64auth] = auth.split(' ');
      if (type == 'Basic') {
        if (b64auth) {
          const [username, password] = Buffer.from(b64auth, 'base64').toString().split(':');
          if (username && password) {
            resolve([username, password]);
          } else {
            reject('Malformed Basic authentication credentials');
          }
        } else {
          reject('Missing Basic authentication credentials');
        }
      } else {
        reject('Invalid authentication scheme, must be Basic authentication');
      }
    } else {
      reject('Missing Basic authentication header');
    }
  });
};